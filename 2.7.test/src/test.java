import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;


public class test {
    //有10个数据，并且数据有重复的，去重
    public static void main1(String[] args) {
        int[] array={1,2,2,3,4,5,6,8,4};
        Set<Integer> set=new HashSet<>();
        for (int i = 0; i <array.length; i++) {
            set.add(array[i]);
        }
        System.out.println(set);
    }
    //有10个数据，并且数据有重复的，找到第一个重复的数据
    public static void main2(String[] args) {
        int[] array={1,2,2,3,4,5,6,8,4};
        Set<Integer> set=new HashSet<>();
        for (int i = 0; i <array.length; i++) {
            if(!set.contains(array[i])) {
                set.add(array[i]);
            }else{
                System.out.println(array[i]);
                return;
            }
        }
    }
//统计10W个数据中，每个数据出现的次数
    public static void main(String[] args) {
  int[] array={1,22,3,4,5,6,8,4};
        Map<Integer,Integer> map=new HashMap<>();
        for (int x: array) {
  if(map.get(x)==null){
      map.put(x,1);
  }else{
      int val=map.get(x);
      map.put(x,val+1);
  }
        }
        for (Map.Entry<Integer,Integer> entry: map.entrySet()) {
            if(entry.getValue()>1){
                System.out.println("key:"+entry.getKey()+" val:"+entry.getKey());
            }
        }
    }


}
