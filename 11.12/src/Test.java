class Student{
   public String name;
   public int age;
   public static String classroom="1班";

    public Student(String name, int age) {
        this.name = name;
        this.age = age;
    }
}
public class Test {
    public static void main(String[] args) {
        Student student=new Student("张三",13);
        System.out.println(student.name+" "+student.age+" "+student.classroom);
        Student student1=new Student("李四",14);
        System.out.println(student1.name+" "+student1.age+" "+student1.classroom);

    }
}
