import java.util.Scanner;
class Base {
    public int x;
    public int y;
    public Base(int x, int y) {
        this.x = x;
        this.y = y;
    }
    public void calculate(int x, int y) {
        System.out.print( x * y);
    }
}
class Sub extends Base {
    public Sub(int x, int y) {
        super(x, y);
    }

    public void calculate(int x, int y) {
        if (y == 0) {
            System.out.println("Error");

        } else {
            System.out.println(x / y)  ;
        }

    }
}
public class Main {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int x = in.nextInt();
        int y = in.nextInt();
        Sub sub = new Sub(x, y);
        sub.calculate(x, y);
    }
}