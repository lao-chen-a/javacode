package com.example.demo.service;

import com.example.demo.dao.ArticleMapper;
import com.example.demo.model.Articleinfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ArticleService {
    @Autowired
    private ArticleMapper articleMapper;
    public List<Articleinfo> getListByUid(int uid){
        return articleMapper.getListByUid(uid);
    }
public int del(Integer aid,int uid){
        return articleMapper.del(aid,uid);
}
public int add(Articleinfo articleinfo){
        return articleMapper.add(articleinfo);
}
public Articleinfo getArticleByIdAndUid(int aid,int uid){
        return articleMapper.getArticleByIdAndUid(aid,uid);
}
public int update(Articleinfo articleinfo){
        return  articleMapper.update(articleinfo);
}
public Articleinfo getDetail(int aid){
        return articleMapper.getDetailById(aid);
}
public int getArtCountByUid(int uid){
        return articleMapper.getArtCountByUid(uid);
}
public int incrementRCount(int aid){
      return articleMapper.incrementRCount(aid);
}
public List<Articleinfo> getListByPage(int psize,int offset){
        return articleMapper.getListByPage(psize,offset);
}
public int getCount(){
        return articleMapper.getCount();
}
}
