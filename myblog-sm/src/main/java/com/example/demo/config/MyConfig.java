package com.example.demo.config;

import org.springframework.boot.autoconfigure.web.servlet.WebMvcAutoConfiguration;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class MyConfig implements WebMvcConfigurer {
    @Override
    public void addInterceptors(InterceptorRegistry registry){
registry.addInterceptor(new LoginIntercept())
        .addPathPatterns("/**")
        .excludePathPatterns("/editor.md/*")
        .excludePathPatterns("/img/*")
        .excludePathPatterns("/js/*")
        .excludePathPatterns("/css/*")
        .excludePathPatterns("blog_list.html")
        .excludePathPatterns("/blog_content.html")
        .excludePathPatterns("reg.html")
        .excludePathPatterns("/login.html")
        .excludePathPatterns("/user/reg")
        .excludePathPatterns("/user/login")
        .excludePathPatterns("/art/reg")
        .excludePathPatterns("/art/getlistbypage");
    }

}
