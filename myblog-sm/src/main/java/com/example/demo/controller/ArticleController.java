package com.example.demo.controller;

import com.example.demo.common.ResultAjax;
import com.example.demo.common.SessionUtils;
import com.example.demo.model.Articleinfo;
import com.example.demo.model.Userinfo;
import com.example.demo.model.vo.UserinfoVO;
import com.example.demo.service.ArticleService;
import com.example.demo.service.UserService;
import com.sun.org.apache.xpath.internal.functions.FuncTrue;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.FutureTask;

@RestController
@RequestMapping("/art")
public class ArticleController {
    @Autowired
    private ArticleService articleService;
    private static final int _DESC_LENGTH = 120;
    @Autowired
    private ThreadPoolTaskExecutor taskExecutor;
    @Autowired
    private UserService userService;
    @RequestMapping("/mylist")
    public ResultAjax myList(HttpServletRequest request) {
        Userinfo userinfo = SessionUtils.getUser(request);
        if (userinfo == null) {
            return ResultAjax.fail(-1, "请先登录");
        }
        List<Articleinfo> list = articleService.getListByUid(userinfo.getId());
        if (list != null && list.size() > 0) {
            list.stream().parallel().forEach((art) -> {
                if (art.getContent().length() > _DESC_LENGTH) {
                    art.setContent(art.getContent().substring(0, _DESC_LENGTH));
                }
            });
        }
        return ResultAjax.succ(list);
    }

    @RequestMapping("/del")
    public ResultAjax del(Integer aid, HttpServletRequest request) {
        if (aid == null || aid <= 0) {
            return ResultAjax.fail(-1, "参数错误!");

        }
        Userinfo userinfo = SessionUtils.getUser(request);
        if (userinfo == null) {
            return ResultAjax.fail(-1, "请先登录");
        }
        int result = articleService.del(aid, userinfo.getId());
        return ResultAjax.succ(result);
    }

    @RequestMapping("/add")
    public ResultAjax add(Articleinfo articleinfo, HttpServletRequest request) {
        if (articleinfo == null || !StringUtils.hasLength(articleinfo.getTitle()) ||
                !StringUtils.hasLength(articleinfo.getContent())) {
            return ResultAjax.fail(-1, "非法参数");
        }
        Userinfo userinfo = SessionUtils.getUser(request);
        if (userinfo == null) {
            return ResultAjax.fail(-2, "请先登录!");
        }
        articleinfo.setUid(userinfo.getId());
        int result = articleService.add(articleinfo);
        return ResultAjax.succ(result);
    }



    @RequestMapping("/update_init")
    public ResultAjax updateInit(Integer aid, HttpServletRequest request) {
        if (aid == null || aid <= 0) {
            return ResultAjax.fail(-1, "参数有无");
        }
        Userinfo userinfo = SessionUtils.getUser(request);
        if (userinfo == null) {
            return ResultAjax.fail(-2, "请先登录!");
        }
        Articleinfo articleinfo = articleService.getArticleByIdAndUid(aid, userinfo.getId());
        return ResultAjax.succ(articleinfo);
    }

    @RequestMapping("/update")
    public ResultAjax update(Articleinfo articleinfo, HttpServletRequest request) {
        if (articleinfo == null ||
                !StringUtils.hasLength(articleinfo.getTitle()) ||
                !StringUtils.hasLength(articleinfo.getContent()) ||
                articleinfo.getId() == 0) {
            return ResultAjax.fail(-1, "非法参数!");
        }
        Userinfo userinfo = SessionUtils.getUser(request);
        if (userinfo == null) {
            return ResultAjax.fail(-2, "请先登录!");
        }
        articleinfo.setUid(userinfo.getId());
        int result = articleService.update(articleinfo);
        return ResultAjax.succ(result);
    }
    @RequestMapping("/detail")
    public  ResultAjax detail(Integer aid) throws ExecutionException, InterruptedException {
        if(aid==null||aid<=0){
            return  ResultAjax.fail(-1,"非法参数");
        }
        Articleinfo articleinfo=articleService.getDetail(aid);
        if(articleinfo==null||articleinfo.getId()<=0){
            return ResultAjax.fail(-1,"非法参数!");
        }
        FutureTask<UserinfoVO> userTask=new FutureTask(()->{
          return userService.getUserById(articleinfo.getUid());

        });
        taskExecutor.submit(userTask);
        FutureTask<Integer> artCountTask=new FutureTask<>(()->{
            return articleService.getArtCountByUid(articleinfo.getUid());
        });
        taskExecutor.submit(artCountTask);
        UserinfoVO userinfoVO= userTask.get();
        int artCount=artCountTask.get();
        userinfoVO.setArtCount(artCount);
        HashMap<String,Object> result=new HashMap<>();
        result.put("user",userinfoVO);
        result.put("art",articleinfo);
        return ResultAjax.succ(result);
    }
    @RequestMapping("/increment_rcount")
    public ResultAjax incrementRCount(Integer aid){
if(aid==null||aid<=0){
    return ResultAjax.fail(-1,"参数有误");
}
int result= articleService.incrementRCount(aid);
return ResultAjax.succ(result);

    }
    @RequestMapping("/getlistbypage")
    public ResultAjax getListByPage(Integer pindex,Integer psize) throws ExecutionException, InterruptedException {
    if(pindex==null||pindex<1){
        pindex=1;
    }
    if(psize==null||psize<1){
        psize=2;
    }
        int finalPsize = psize;
        int finalOffset= psize *(pindex -1);
        FutureTask<List<Articleinfo>> listTask=new FutureTask<>(()->{
return articleService.getListByPage(finalPsize,finalOffset);
    });
    FutureTask<Integer> sizeTask=new FutureTask<>(()->{
      int totalCount=articleService.getCount();
      double sizeTemp=(totalCount*1.0)/(finalPsize*1.0);
        return (int)Math.ceil(sizeTemp);
    });
    taskExecutor.submit(listTask);
    taskExecutor.submit(sizeTask);
    List<Articleinfo> list=listTask.get();
    int size=sizeTask.get();
    HashMap<String,Object> map=new HashMap<>();
    map.put("list",list);
    map.put("size",size);
        return ResultAjax.succ(map);
    }
}