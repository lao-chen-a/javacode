import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

public class Main {
    public static void main(String[] args)throws IOException {
        try (InputStream file=new FileInputStream("D:/test/hello.txt")){
                byte[] buf=new  byte[1024];
                int len;
                while (true){
                    len=file.read(buf);
                    if(len==-1){
                        break;
                }
                    //每次使用3字节进行utf-8解码，得到中文字符
                    //利用String中的构造方法完成
                    //了解即可，不是通用解决办法
                    for (int i = 0; i < len; i+=3) {
                        String s=new String(buf,i,3,"UTF-8");
                        System.out.printf("%s",s);

                    }
            }
        }
    }
}