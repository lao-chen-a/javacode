public  class TestBinaryTree {
static  class  TreeNode {
    public char val;//数据域
    public TreeNode left;//左孩子节点
    public TreeNode right;//右孩子节点
    public TreeNode(char val) {
        this.val = val;
    }
}
    public TreeNode createTree( ){
        TreeNode A=new TreeNode('A');
        TreeNode B=new TreeNode('B');
        TreeNode C=new TreeNode('C');
        TreeNode D=new TreeNode('D');
        TreeNode E=new TreeNode('E');
        TreeNode F=new TreeNode('F');
        TreeNode G=new TreeNode('G');
        TreeNode H=new TreeNode('H');
        A.left=B;
        A.right=C;
        B.left=D;
        B.right=E;
        C.left=F;
        C.right=G;
        E.right=H;
        return A;
    }
public void preOrder(TreeNode root){
        //当二叉树根结点为空
    if(root==null){
        return;
    }
    //不为空，打印树的根结点的值
    System.out.print(root.val+" ");
    //这时对左子树进行先序遍历，又是新的二叉树
    preOrder(root.left);
    preOrder(root.right);

}
void inOrder(TreeNode root){
        if(root==null){
    return;
}
        inOrder(root.left);
    System.out.print(root.val+" ");
        inOrder(root.right);

}
void postOrder(TreeNode root){
    if(root==null){
        return;
    }
    inOrder(root.left);
    inOrder(root.right);
    System.out.print(root.val+" ");
}
//获取树中节点的个数
    int size(TreeNode root){
    int count=0;
    if(root==null){
        return count;
    }
    int sizeLeft=size(root.left);
    int sizeRight=size(root.right);
    return sizeLeft+sizeRight+1;
    }
    int getLeafNodeCount(TreeNode root){
        if (root==null){
            return 0;
        }
     if(root.left==null&&root.right==null){
         return 1;
     }
          int leafLeft=getLeafNodeCount(root.left);
          int leafRight=getLeafNodeCount(root.right);
          return leafLeft+leafRight;
}
// 获取第K层节点的个数
    int getLevelNodeCount(TreeNode root,int k){
     if(root==null){
         return 0;
     }
    if(k==1){
        return 1;
    }
    int levelLeft=getLevelNodeCount(root.left,k-1);
    int levelRight=getLevelNodeCount(root.right,k-1);
    return levelLeft+levelRight;
    }
    // 获取二叉树的高度
    int getHeight(TreeNode root){
    if (root==null){
        return 0;
    }
    int leftHeight=getHeight(root.left);
    int rightHeight=getHeight(root.right);
    return (leftHeight>rightHeight)?(leftHeight+1):(rightHeight+1);
    }
    //检测值为value的元素是否存在
    TreeNode  find(TreeNode root,int val){
    if(root==null){
        return null;
    }
    if(root.val==val){
        return root;
    }
    TreeNode left=find(root.left,val);
    if(left!=null){
        return left;
    }
    TreeNode right=find(root.right,val);
    if (right!=null){
        return right;
    }
    return null;
    }
    }

