public class Test {
    public static void main(String[] args) {
        TestBinaryTree testBinaryTree=new TestBinaryTree();
        TestBinaryTree.TreeNode root = testBinaryTree.createTree();
    testBinaryTree.preOrder(root);
        System.out.println();
    testBinaryTree.inOrder(root);
        System.out.println();
    testBinaryTree.postOrder(root);
        System.out.println();
        System.out.println(testBinaryTree.size(root));
        System.out.println(testBinaryTree.getLeafNodeCount(root));
        System.out.println(testBinaryTree.getLevelNodeCount(root,4));
        System.out.println(testBinaryTree.getHeight(root));
        System.out.println(testBinaryTree.find(root,'B'));
    }
}
