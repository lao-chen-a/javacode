//把这个类设定为单例
class Singleton{
    //唯一的实例的实体
private static Singleton instance=new Singleton();
//被static修饰，该属性是类的属性。JVM中，每个类的类对象只有唯一一份，类对象里的这个成员自然也是唯一一份了。
//获取到实例的方法
    public static Singleton getInstance(){
        return instance;
    }
    //禁止外部new实例
    private Singleton(){};
}
public class test {
    public static void main(String[] args) {
    Singleton s1=Singleton.getInstance();
    Singleton s2=Singleton.getInstance();

    }

    }

