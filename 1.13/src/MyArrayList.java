import java.util.Arrays;

public class MyArrayList {
    int[] elem;
    public int usesize=3;
    public static final int DEFAULT=10;
  public MyArrayList(){
      this.elem=new int[DEFAULT];
  }
    //输出顺序表
  public void display(int[] elem){
      for (int i=0;i<this.usesize;i++){
          System.out.print(this.elem[i]);
      }
  }
  //得到顺序表长度
public int getUsesize(){
      return this.usesize;
}
//判断顺序表中是否有该值
public boolean contains(int toFind){
      for (int i=0;i<this.usesize;i++){
          if (elem[i]==toFind){
              return true;
          }
      }
      return false;
}
//查找该数在顺序表的位置
public int indexOf(int toFind){
      for(int i=0;i<this.usesize;i++){
          if(elem[i]==toFind){
              return i;
          }

      }
      return -1;
}
//添加元素
public void add(int data){
      if(isFull(elem)) {
          this.elem=Arrays.copyOf(this.elem,2*elem.length);
      }
          this.elem[this.usesize] = data;
          this.usesize++;
}
//判断是否满
public  boolean isFull(int[] elem){
      if (usesize==DEFAULT){
          return true;
      }
      return false;
}
//在pos位置放置data元素
public void set(int pos,int data){
      this.elem[pos]=data;
}
//判断位置是否合法
private  void checkAddIndex(int pos){
      if(pos<0||pos>usesize){
          throw new AddIndexOutOfException
                  ("add元素位置不合法");
      }
}
//增加某元素
public void add(int pos,int data){
checkAddIndex(pos);
      isFull(elem);

      for(int i=this.usesize-1;i>=pos;i--){
         elem[i+1]=elem[i];
      }
      elem[pos]=data;
      usesize++;
}
//得到在该位置的数
public int get(int pos){
      checkAddIndex(pos);
      return elem[pos];
}
//移除某元素
public boolean remove(int toRemove){
      int index=indexOf(toRemove);
      if(index==-1){
          System.out.println("没有这个数");
          return false;
      }
      for (int i=index;i<usesize-1;i++){
          elem[i]=elem[i+1];
      }
      usesize--;
      elem[usesize]=0;
      return true;
}
//清空顺序表
public void clear(){
      usesize=0;
}
}
