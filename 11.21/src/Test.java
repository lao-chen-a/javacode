class Outer {
    public int a1=1;
     class Inner{
      static final   public  int a1=2;//加上final就相当于常量。否则会报错。因为static静态类是不依赖对象的。
       public void func(){
      Outer outer=new Outer();
           System.out.println(outer.a1);
           System.out.println(Inner.this.a1);
     }
    }
}
public class Test{
    public static void main(String[] args) {
Outer outer=new Outer();
Outer.Inner inner=outer.new Inner();
inner.func();
    }
}