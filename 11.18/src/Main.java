abstract class Shape{
   public abstract void shape();
}
class Star extends Shape{
    @Override
    public void shape() {
        System.out.println("⭐");
    }
}
class Heart extends Shape{
    @Override
    public void shape() {
        System.out.println("❤");
    }
}
class Sun extends Shape{
    @Override
    public void shape() {
        System.out.println("☀");
    }
}
class Main{
    public static void main(String[] args) {
Shape shape=new Star();
shape.shape();

    }
}