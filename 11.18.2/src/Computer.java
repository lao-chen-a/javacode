interface USB{//接口
    void openDevice();
    void closeDevice();
}
 class Mouse implements USB{
    @Override
    public void openDevice() {
        System.out.println("开机");
    }
     public void closeDevice() {
         System.out.println("关机");
     }
}
public class Computer {
    public static void control(USB usb){
        usb.openDevice();
        usb.closeDevice();
    }
    public static void main(String[] args) {
    USB usb1=new Mouse();
    control(usb1);
    }
}
