public class Test {
            public static void main(String[] args) {
                int a = 10;
                int b = 20;
                swap(a, b);
                System.out.println("main: a = " + a + " b = " + b);
            }
            public static void swap(int x, int y) {
                int tmp = x;
                x = y;
                y = tmp;
                System.out.println("swap: x = " + x + " y = " + y);
            }
    }

