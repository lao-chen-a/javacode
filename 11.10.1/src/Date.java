public class Date {
    public int year;
    public int month;
    public int day;
    public Date(int year,int month,int day){
        this.year=year;
        this.month=month;
        this.day=day;
    }
    public void printDate(){
        System.out.println(year+"-"+month+"-"+day);
    }
    public static void main(String[] args) {
     Date d=new Date(2021,6,9);
        d.printDate();
    }
}
