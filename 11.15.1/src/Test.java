class Base {
 static {
     System.out.println("执行父类静态代码块");
 }
    {
        System.out.println("执行父类实例代码块");
    }
    public Base() {
        System.out.println("执行父类构造代码块");
    }
}
class  Derived extends Base {
   static {
       System.out.println("执行子类静态代码块");
   }
    {
        System.out.println("执行子类实例代码块");
    }
    public Derived() {
        System.out.println("执行子类构造代码块");
    }
}
public class Test {
    public static void main(String[] args) {
        Derived derived=new Derived();
    }
}
