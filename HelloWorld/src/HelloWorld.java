public class HelloWorld {
        public static void main(String[] args) {
            int a = 10;//可以修改,整形
            final  int b=20;//不可修改
            double c=3.14;//双精度浮点型
            char d='A';//字符型
            boolean e=true;//布尔型
            System.out.println(a);//相当于C语言中的printf,ln这里是换行
            System.out.println(b);
            System.out.println(c);
            System.out.println(d);
            System.out.println(e);
            int a1=1,a2=2,a3=3;
            System.out.println(a1);
            System.out.println(a2);
            System.out.println(a3);
        }
}
