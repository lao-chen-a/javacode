class Animal{
 String name;
    public Animal(String name) {
        this.name = name;
    }
    public void fly(){
        System.out.println(name+"会飞");
    }
    public void run(){
        System.out.println(name+"会跑");
    }
}
interface Irunning{
    void run();
}
interface Iflying{
    void fly();
}
interface Isiwmming{
    void swim();
}
class Bird extends Animal implements Iflying {
    public Bird(String name) {
        super(name);
    }
 public void fly(){
     System.out.println(name+"会飞");
 }
}
class Rabbit extends Animal implements Irunning {
    public Rabbit(String name) {
        super(name);
    }
    public void run(){
    System.out.println(name+"会跑");
}
}
class Flog extends Animal implements Irunning,Isiwmming{
    public Flog(String name) {
        super(name);
    }
    public void run(){
        System.out.println(name+"会跑");
    }
    public void swim(){
        System.out.println(name+"会游泳");
    }
}
public class Test {
    public static void fly(Bird bird){
        bird.fly();
    }
    public static void run(Rabbit rabbit){
        rabbit.run();
    }
    public static void run(Flog flog){
        flog.run();
    }
    public static void swim(Flog flog){
        flog.swim();
    }
    public static void main(String[] args) {
Bird bird=new Bird("小鸟");
fly(bird);
Rabbit rabbit=new Rabbit("小兔子");
run(rabbit);
Flog flog=new Flog("小青蛙");
run(flog);
swim(flog);
    }
}
