package com.example.mq.common;

import java.io.*;

public class BinaryTool {
    public static byte[] toBytes(Object object)throws IOException {
      try(ByteArrayOutputStream byteArrayOutputStream=new ByteArrayOutputStream()){
        try(ObjectOutputStream objectOutputStream=new ObjectOutputStream(byteArrayOutputStream)){
            objectOutputStream.writeObject(object);
        }
        return byteArrayOutputStream.toByteArray();
    }
    }
    public static  Object fromBytes(byte[] data)throws IOException,ClassNotFoundException{
        Object object=null;
try (ByteArrayInputStream byteArrayInputStream=new ByteArrayInputStream(data)){
try (ObjectInputStream objectInputStream=new ObjectInputStream(byteArrayInputStream)){
object=objectInputStream.readObject();
}
}
return object;
    }
}
