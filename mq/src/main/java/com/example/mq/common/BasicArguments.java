package com.example.mq.common;

import java.io.Serializable;

public class BasicArguments implements Serializable {
    protected String rid;
    protected String channelId;
    public String getRid() {
        return rid;
    }

    public void setRid(String rid) {
        this.rid = rid;
    }

    public String getChannelId() {
        return channelId;
    }

    public void setChannelId(String channelId) {
        this.channelId = channelId;
    }
}
