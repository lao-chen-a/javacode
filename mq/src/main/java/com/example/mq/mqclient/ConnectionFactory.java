package com.example.mq.mqclient;

import java.io.IOException;

public class ConnectionFactory {
    private String host;
    private int port;

    public Connection newConnection() throws IOException {
        Connection connection=new Connection(host,port);
        return connection;
    }
    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }
}
