import com.bit.demo.UserService;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.xml.XmlBeanFactory;
import org.springframework.core.io.ClassPathResource;

public class App2 {
    public static void main(String[] args) {
        //得到Spring上下文对象
        BeanFactory context=new XmlBeanFactory(
                new ClassPathResource("spring-config.xml"));
        //获取Bean
        UserService userService=(UserService) context.getBean("user");
        //使用Bean
        userService.sayHi();
    }
}
