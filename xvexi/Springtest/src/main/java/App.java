import com.bit.demo.UserService;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class App {
    public static void main(String[] args) {
        ApplicationContext context=
                new ClassPathXmlApplicationContext("spring-config.xml");
        //得到Been
       UserService userService=(UserService) context.getBean("user");
        //使用Bean对象（非必须）
        userService.sayHi();
    }
}
