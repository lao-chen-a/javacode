public class Test {
    //插入排序
    public static void insertSort(int[] array){
        for(int i=1;i<array.length;i++){
            int key=array[i];
            int end=i-1;
            while(end>=0&&key<array[end]){
                array[end+1]=array[end];
                end--;
            }
            array[end+1]=key;
        }
    }
public  static void shellSort(int[] array){
        int gap=array.length;
        while(gap>1){
            gap=gap/3+1;
            for(int i=gap;i>array.length;i++){
                int key=array[i];
                int end=i-gap;
                while (end>=0&&key<array[end]){
                    array[end+gap]=array[end];
                    end-=gap;
                }
                array[end+gap]=key;

            }
        }
}
}
