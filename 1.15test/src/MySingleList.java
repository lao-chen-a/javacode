import jdk.nashorn.internal.ir.WhileNode;

public class MySingleList {
   static class Node{
        public int val;//存储的数据
        public Node next;//存储下一个节点的地址
        //构造方法
        public Node(int val){
            this.val=val;
        }
    }

    public Node head;//代表当前链表的头节点的引用
     public  void createLink( )
    {
        Node node1=new Node(1);
        Node node2=new Node(1);
        Node node3=new Node(0);
        Node node4=new Node(0);
        node1.next=node2;
        node2.next=node3;
        node3.next=node4;
        head=node1;

}
/*
* 遍历链表*/

    public void display( ){
        Node cur=head;
        while (cur!=null){
            System.out.print(cur.val+" ");
           cur=cur.next;
        }
        System.out.println();

    }
    //判断key值是否在链表
    public  boolean contains(int key){
        Node cur=head;
        while(cur!=null){
            if(cur.val==key){
            return true;
        }
            cur=cur.next;
        }
        return false;
    }
    //得到链表的长度
    public int size(){
            int i=0;
            Node cur=head;
            while (cur!=null){
                i++;
                cur=cur.next;
            }
            return i;
    }
    //头插法O(1)
    public void  addFirst(int data){
        Node node=new Node(data); //新建立了一个头节点
        node.next=head;
        head=node;
    }
    //尾插法O(N)
public  void addLast(int data){
    Node node=new Node(data);
    if(head==null){
        head=node;
        return;
    }
    Node cur=head;
    while (cur.next!=null){
        cur=cur.next;
    }
    cur.next=node;
}
//任意位置插入，第一个数据节点为0号下标
public void addIndex(int index,int data)
        throws ListIndexOutOfException{
    checkIndex(index);
    if(index == 0) {
        addFirst(data);
        return;
    }
    if(index == size()) {
        addLast(data);
        return;
    }
    Node cur = findIndexSubOne(index);
    Node node = new Node(data);
    node.next = cur.next;
    cur.next = node;
}

    /**
     * 找到 index-1位置的节点的地址
     * @param index
     * @return
     */
    private Node findIndexSubOne(int index) {
        Node cur = head;
        int count = 0;
        while (count != index-1) {
            cur = cur.next;
            count++;
        }
        return cur;
    }
    private void checkIndex(int index) throws ListIndexOutOfException{
        if(index < 0 || index > size()) {
            throw new ListIndexOutOfException("index位置不合法");
        }
    }
public  void remove(int key){
        if(head==null){
            return;
        }
        if(head.val==key){
            head=head.next;
            return;
        }
        Node cur=searchPrev(key);
        if(cur==null){
            return;
        }
    Node del = cur.next;//要删除的节点
    cur.next = del.next;
}
private  Node searchPrev(int key) {
    Node cur = head;
    while (cur.next != null) {
        if (cur.next.val == key) {
            return cur;
        }
        cur = cur.next;
    }
    return null;
}
public void removeAllKey(int key){
    if(head == null) {
        return;
    }
        /*while(head.val == key) {
            head = head.next;
        }*/
    Node prev = head;
    Node cur = head.next;
    while (cur != null) {
        if(cur.val == key) {
            prev.next = cur.next;
            cur = cur.next;
        }else {
            prev = cur;
            cur = cur.next;
        }
    }
    if(head.val == key) {
        head = head.next;
    }
}
    public void clear() {
        head = null;
    }

    }


