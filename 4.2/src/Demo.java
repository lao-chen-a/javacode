public class Demo {
    public static void main(String[] args)throws InterruptedException {
        Thread tc=new Thread(()->{
            System.out.print(Thread.currentThread().getName()+" ");
        },"c");
        Thread tb=new Thread(()->{
            try {
                tc.join();
            }catch (InterruptedException e){
                e.printStackTrace();
            }
            System.out.print(Thread.currentThread().getName()+" ");
        },"b");
        Thread ta=new Thread(()->{
            try {
                tb.join();
            }catch (InterruptedException e){
                e.printStackTrace();
            }
            System.out.print(Thread.currentThread().getName()+" ");
        },"a");
        ta.start();
        tb.start();
        tc.start();

    }

}
