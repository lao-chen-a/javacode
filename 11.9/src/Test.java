import java.util.Arrays;

public class Test {
    public static boolean threeOdd(int[] array) {
        int count=0;
        for (int i = 0; i < array.length; i++) {
           if(array[i]%2!=0){
               count++;
               if(count==3){
                return true;
               }
           }else {
               count=0;
           }

        }return false;
    }
    public static void main(String[] args) {
        int[] array=new int[]{2,6,4,1};
        boolean ret=threeOdd(array);
        System.out.println(ret);
    }

}
