package threading;
class Counter{
    private int count=0;
    private Object locker=new Object();
    public void add(){
    synchronized (locker){
        count++;
    }

    }
    public int get(){
        return count;
    }
}
public class ThreadDemo10 {
    public static void main(String[] args) throws InterruptedException{
        Counter counter=new Counter();
        //搞两个线程，两个线程分别对这个counter自增5w次
        Thread t1=new Thread(()->{
            for (int i = 0; i <10000 ; i++) {
                counter.add();
            }
        });
        t1.start();
        Thread t2=new Thread(()-> {
            for (int i = 0; i < 10000; i++) {
                counter.add();
            }
        });
        t2.start();
        t1.join();
        t2.join();
        System.out.println(counter.get());
    }
}
