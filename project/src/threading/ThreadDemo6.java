package threading;
public class ThreadDemo6 {
    public static void main(String[] args) {
        boolean isQuit=false;
        Thread t=new Thread(()-> {
            while (!isQuit){
                System.out.println("hello t");
                try{
                    Thread.sleep(1000);//休息1秒
                }catch (InterruptedException e){
                    e.printStackTrace();
                }
            }
            System.out.println("t 线程终止");
        });
        t.start();
        //在主线程中，修改isQuit
//        try {
//            Thread.sleep(3000);//主线程休息三秒
//        }catch (InterruptedException e){
//            e.printStackTrace();
//        }
//        isQuit=true;//将isQuit值改成true
    }
}
