package threading;

import java.util.WeakHashMap;

public class ThreadDemo5 {
    public static void main(String[] args) {
        Thread thread=new Thread(()->{
            while (true){
                System.out.println("hello t");
                try{
                    Thread.sleep(1000);
                }catch (InterruptedException e){
                    e.printStackTrace();
                }
            }
        });
        thread.start();
        while (true){
            System.out.println("hello main");
            try{
                Thread.sleep(1000);
            }catch (InterruptedException e){
                e.printStackTrace();
            }
        }
    }
}
