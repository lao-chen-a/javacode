package threading;
class MyThread extends Thread{
    @Override
    //重写
    public void run(){
        while (true){
            System.out.println("hello t");

        }

    }
}
public class ThreadDemo1 {
    public static void main(String[] args) {
        Thread t=new MyThread();//先创建MyThread实例。t的引用实际上是指向子类的实例
        t.run();//run不会创建新的线程，run是在main线程中执行的
    while (true){
        System.out.println("hello main");
    }
     }
}
