package threading;

public class ThreadDemo9 {
    public static void main(String[] args)throws InterruptedException {
        Thread t=new Thread(()->{
            while (true){
                try {
                    Thread.sleep(1000);
                }catch (InterruptedException e){
                    e.printStackTrace();
                }
            }
        });
        System.out.println(t.getState());
        t.start();
        Thread.sleep(2000);
        System.out.println(t.getState());
    }
}
