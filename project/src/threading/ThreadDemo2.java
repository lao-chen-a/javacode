package threading;
class MyRunnable implements Runnable{
    @Override
    //Runnable字面意思是可运行的。使用Runnable描述一个具体的任务，通过run方法来描述。
    public void run(){
while (true){
    System.out.println("hello t");
    try {
        Thread.sleep(1000);
    }catch (InterruptedException e){
        e.printStackTrace();
    }
}
    }
}
public class ThreadDemo2 {
    public static void main(String[] args) {
        MyRunnable runnable=new MyRunnable();
Thread t=new Thread(runnable);
t.start();
while (true){
    System.out.println("hello main");
    try {
        Thread.sleep(1000);
    }catch (InterruptedException e){
        e.printStackTrace();
    }
}
    }
}
