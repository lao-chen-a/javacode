package threading;
public class ThreadDemo7 {
    public static void main(String[] args) {
        Thread t=new Thread(()->{
            //currentThread是获取当前线程实例
            //此处currentThread得到的对象就是t
            //isInterrupted是t对象里自带的一个标志位
          while (!Thread.currentThread().isInterrupted()){
              System.out.println("hello t");
              try {
                  Thread.sleep(1000);
              }catch (InterruptedException e){
                  e.printStackTrace();//打印出当前异常位置的调用栈
                  break;
              }
              }
        });
        t.start();
        try{
            Thread.sleep(3000);
        }catch (InterruptedException e){
            e.printStackTrace();
        }
        //把t内部的标志位设置成true
        //如果该进程阻塞中，就会把阻塞状态唤醒，通过抛出异常的方式，让sleep结束。
        t.interrupt();
    }
}
