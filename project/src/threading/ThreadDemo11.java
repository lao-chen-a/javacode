package threading;

import java.util.Scanner;

public class ThreadDemo11 {
  volatile public static int flag=0;
    public static void main(String[] args)  {

        Thread t1=new Thread(()->{
            while (flag==0) {

            }
            System.out.println("循环结束！t1结束！");
       });
        Thread t2=new Thread(()->{
            Scanner scanner=new Scanner(System.in);
            System.out.println("请输入一个整数");
            flag=scanner.nextInt();
        });
        t1.start();
        t2.start();
    }
}
