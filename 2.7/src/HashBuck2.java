public class HashBuck2<K,V> {
    static class Node<K,V>{
        public  K key;
        public  V value;
        public Node<K,V> next;
        public Node(K key,V value){
            this.key=key;
            this.value=value;
        }
    }
    public  Node<K,V>[] array=(Node<K, V>[])new Node[10];
    public int usedSize;
    public void put(K key,V value){
        int hash=key.hashCode();
        int index=hash%array.length;
        Node<K,V> cur=array[index];
        while(cur!=null){
            if(cur.equals(key)){
                cur.value=value;
                return;
            }
            cur=cur.next;
        }
        Node<K,V> node=new Node<>(key,value);
        node.next=array[index];
        array[index]=node;
        usedSize++;
    }
public V get(K key){
        int hash=key.hashCode();
        int index=hash%array.length;
        Node<K,V> cur=array[index];
        while(cur!=null){
            if(cur.key.equals(key)){
                return  cur.value;
            }
            cur=cur.next;
        }
        return null;
}

}
