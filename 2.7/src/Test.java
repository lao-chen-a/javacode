import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

public class Test {
    public static void main(String[] args) {
        HashBuck hashBuck=new HashBuck();
        hashBuck.put(1,11);
        hashBuck.put(2,22);
        hashBuck.put(5,55);
        hashBuck.put(8,88);
        Integer val=hashBuck.get(5);
        System.out.println(val);

    }
    public static void main2(String[] args) {
        Set<String> set1=new TreeSet<>();
        set1.add("hello");
        set1.add("abc");
        set1.add("abc");
        System.out.println(set1);
    }
    public static void main1(String[] args) {
        Map<String,Integer> treeMap=new TreeMap<>();
        treeMap.put("abc",3);
        treeMap.put("the",2);
        System.out.println(treeMap);
        Integer val=treeMap.get("abc");
        System.out.println(val);
        Integer val1=treeMap.getOrDefault("hello",1);
        System.out.println(val1);
        Set<String> keySet=treeMap.keySet();
        System.out.println(keySet);
        Set<Map.Entry<String,Integer>> set=treeMap.entrySet();
   for(Map.Entry<String,Integer> entry:set){
       System.out.println("key:"+entry.getKey()+"value"+entry.getValue());
   }
    }
}
