package Blog;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import static java.lang.Thread.sleep;

public class LoginTests extends Screen {
    //账号密码正确
    @Test
    void LoginSuccess() throws InterruptedException {
        WebDriver webDriver=new ChromeDriver();
        webDriver.get("http://124.223.101.247:8080/login.html");
        sleep(300);
        webDriver.findElement(By.cssSelector("#username")).sendKeys("chen");
        webDriver.findElement(By.cssSelector("#password")).sendKeys("12");
        webDriver.findElement(By.cssSelector("#submit")).click();
        sleep(300);
        String cur_url=webDriver.getCurrentUrl();
        Assertions.assertEquals("http://124.223.101.247:8080/myblog_list.html",cur_url);
        taskScreenShot(webDriver);
        webDriver.quit();
    }
    //账号密码不正确
    @Test
    public void loginFalse1() throws InterruptedException {
        WebDriver webDriver=new ChromeDriver();
        webDriver.get("http://124.223.101.247:8080/login.html");
        webDriver.findElement(By.cssSelector("#username")).sendKeys("chen");
        webDriver.findElement(By.cssSelector("#password")).sendKeys("12345");
        webDriver.findElement(By.cssSelector("#submit")).click();
        sleep(300);
        System.out.println(webDriver.switchTo().alert().getText());
        webDriver.switchTo().alert().accept();
        taskScreenShot(webDriver);
        webDriver.quit();
    }
    //测试账号为空
    @Test
    public void loginFalse2() throws InterruptedException {
        WebDriver webDriver=new ChromeDriver();
        webDriver.get("http://124.223.101.247:8080/login.html");
        webDriver.findElement(By.cssSelector("#username")).sendKeys("");
        webDriver.findElement(By.cssSelector("#password")).sendKeys("12345");
        webDriver.findElement(By.cssSelector("#submit")).click();
        sleep(300);
        System.out.println(webDriver.switchTo().alert().getText());
        webDriver.switchTo().alert().accept();
        webDriver.quit();
    }
    @Test
    public void loginFalse3() throws InterruptedException {
        WebDriver webDriver=new ChromeDriver();
        webDriver.get("http://124.223.101.247:8080/login.html");
        webDriver.findElement(By.cssSelector("#username")).sendKeys("chen");
        webDriver.findElement(By.cssSelector("#password")).sendKeys("");
        webDriver.findElement(By.cssSelector("#submit")).click();
        sleep(300);
        System.out.println(webDriver.switchTo().alert().getText());
        webDriver.switchTo().alert().accept();
        webDriver.quit();
    }
    //测试回车键
    @Test
    public void loginFalse4() throws InterruptedException {
        WebDriver webDriver=new ChromeDriver();
        webDriver.get("http://124.223.101.247:8080/login.html");
        webDriver.findElement(By.cssSelector("#username")).sendKeys("chen");
        webDriver.findElement(By.cssSelector("#password")).sendKeys("12");
        webDriver.findElement(By.cssSelector("#password")).sendKeys(Keys.ENTER);
        sleep(300);
        String cur_url=webDriver.getCurrentUrl();
        Assertions.assertEquals("http://124.223.101.247:8080/login.html",cur_url);
        webDriver.quit();
    }
}
