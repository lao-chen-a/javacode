package Blog;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class RegTests {
    @Test
    public void regSuccess(){
        WebDriver webDriver=new ChromeDriver();
        webDriver.get("http://124.223.101.247:8080/reg.html");
        webDriver.findElement(By.cssSelector("#username")).sendKeys("XiaoChen2");
        webDriver.findElement(By.cssSelector("#password")).sendKeys("12345");
        webDriver.findElement(By.cssSelector("#password2")).sendKeys("12345");
        webDriver.findElement(By.cssSelector("#submit")).click();
//        webDriver.get("http://124.223.101.247:8080/login.html");
//        webDriver.findElement(By.cssSelector("#username")).sendKeys("XiaoChen2");
//        webDriver.findElement(By.cssSelector("#password")).sendKeys("12345");
//        webDriver.findElement(By.cssSelector("#submit")).click();
       webDriver.quit();
    }
    //用户名为空
    @Test
    public void regFalse1(){
        WebDriver webDriver=new ChromeDriver();
        webDriver.get("http://124.223.101.247:8080/reg.html");
        webDriver.findElement(By.cssSelector("#username")).sendKeys("");
        webDriver.findElement(By.cssSelector("#password")).sendKeys("12345");
        webDriver.findElement(By.cssSelector("#password2")).sendKeys("12345");
        webDriver.findElement(By.cssSelector("#submit")).click();
        System.out.println(webDriver.switchTo().alert().getText());
        webDriver.quit();
    }
    //密码为空
    @Test
    public void regFalse2(){
        WebDriver webDriver=new ChromeDriver();
        webDriver.get("http://124.223.101.247:8080/reg.html");
        webDriver.findElement(By.cssSelector("#username")).sendKeys("XiaoChen");
        webDriver.findElement(By.cssSelector("#password")).sendKeys("");
        webDriver.findElement(By.cssSelector("#password2")).sendKeys("12345");
        webDriver.findElement(By.cssSelector("#submit")).click();
        System.out.println(webDriver.switchTo().alert().getText());
        webDriver.quit();
    }
    //确认密码为空
    @Test
    public void regFalse3(){
        WebDriver webDriver=new ChromeDriver();
        webDriver.get("http://124.223.101.247:8080/reg.html");
        webDriver.findElement(By.cssSelector("#username")).sendKeys("XiaoChen");
        webDriver.findElement(By.cssSelector("#password")).sendKeys("12345");
        webDriver.findElement(By.cssSelector("#password2")).sendKeys("");
        webDriver.findElement(By.cssSelector("#submit")).click();
        System.out.println(webDriver.switchTo().alert().getText());
        webDriver.quit();
    }
    //密码和确认密码不一致
    @Test
    public void regFalse4(){
        WebDriver webDriver=new ChromeDriver();
        webDriver.get("http://124.223.101.247:8080/reg.html");
        webDriver.findElement(By.cssSelector("#username")).sendKeys("XiaoChen");
        webDriver.findElement(By.cssSelector("#password")).sendKeys("12345");
        webDriver.findElement(By.cssSelector("#password2")).sendKeys("123");
        webDriver.findElement(By.cssSelector("#submit")).click();
        System.out.println(webDriver.switchTo().alert().getText());
        webDriver.quit();
    }
}
