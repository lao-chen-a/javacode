package Blog;

import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import static java.lang.Thread.sleep;

public class BlogListTests extends Screen {
    @Test
   void pagingTest() throws InterruptedException {
       WebDriver webDriver=new ChromeDriver();
       webDriver.get("http://124.223.101.247:8080/login.html");
       sleep(300);
       webDriver.findElement(By.cssSelector("#username")).sendKeys("chen");
       webDriver.findElement(By.cssSelector("#password")).sendKeys("12");
       webDriver.findElement(By.cssSelector("#submit")).click();
       sleep(300);
       webDriver.findElement(By.cssSelector("body > div.nav > a:nth-child(4)")).click();
       //测试能否提示已经是首页
       webDriver.findElement(By.cssSelector("body > div.container > div > div.blog-pagnation-wrapper > button:nth-child(2)")).click();
        webDriver.switchTo().alert().getText();
        webDriver.switchTo().alert().accept();
        sleep(300);
        webDriver.findElement(By.cssSelector("body > div.container > div > div.blog-pagnation-wrapper > button:nth-child(3)")).click();
        taskScreenShot(webDriver);
    }
}
