package Blog;

import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import static java.lang.Thread.sleep;

public class EditTests extends Screen {
    @Test
    void EditSuccess() throws InterruptedException {
        WebDriver webDriver = new ChromeDriver();
        webDriver.get("http://124.223.101.247:8080/login.html");
        sleep(300);
        webDriver.findElement(By.cssSelector("#username")).sendKeys("chen");
        webDriver.findElement(By.cssSelector("#password")).sendKeys("12");
        webDriver.findElement(By.cssSelector("#submit")).click();
        sleep(300);
        webDriver.findElement(By.xpath("/html/body/div[1]/a[2]")).click();
        webDriver.findElement(By.cssSelector(("#title"))).sendKeys("今天天气很好");
        //webDriver.findElement(By.cssSelector("#editorDiv > div.CodeMirror.cm-s-default.CodeMirror-wrap > div.CodeMirror-scroll > div.CodeMirror-sizer > div > div > div > div.CodeMirror-code > div > pre > span > span")).click();
        sleep(300);
        webDriver.findElement(By.xpath("/html/body/div[2]/div[1]/button")).click();
        webDriver.quit();
    }
    //未输入标题
    @Test
    void EditFalse1() throws InterruptedException {
        WebDriver webDriver = new ChromeDriver();
        webDriver.get("http://124.223.101.247:8080/login.html");
        sleep(300);
        webDriver.findElement(By.cssSelector("#username")).sendKeys("chen");
        webDriver.findElement(By.cssSelector("#password")).sendKeys("12");
        webDriver.findElement(By.cssSelector("#submit")).click();
        sleep(300);
        webDriver.findElement(By.xpath("/html/body/div[1]/a[2]")).click();
        sleep(300);
        webDriver.findElement(By.xpath("/html/body/div[2]/div[1]/button")).click();
        System.out.println(webDriver.switchTo().alert().getText());
        webDriver.quit();
    }
    //标题输入空格
    @Test
    void EditFalse2() throws InterruptedException {
        WebDriver webDriver = new ChromeDriver();
        webDriver.get("http://124.223.101.247:8080/login.html");
        sleep(300);
        webDriver.findElement(By.cssSelector("#username")).sendKeys("chen");
        webDriver.findElement(By.cssSelector("#password")).sendKeys("12");
        webDriver.findElement(By.cssSelector("#submit")).click();
        sleep(300);
        webDriver.findElement(By.xpath("/html/body/div[1]/a[2]")).click();
        sleep(300);
        webDriver.findElement(By.cssSelector(("#title"))).sendKeys(Keys.SPACE);
        webDriver.findElement(By.xpath("/html/body/div[2]/div[1]/button")).click();
        System.out.println(webDriver.switchTo().alert().getText());
        webDriver.quit();
    }
}
