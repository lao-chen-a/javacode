package Blog;

import org.apache.commons.io.FileUtils;
import org.junit.jupiter.api.*;
import org.openqa.selenium.*;

import java.io.File;
import java.io.IOException;

import static java.lang.Thread.sleep;
import static org.junit.jupiter.api.Assertions.assertEquals;
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class Screen {
    public  void taskScreenShot(WebDriver webDriver){
        long date = System.currentTimeMillis();
        String path = String.valueOf(date);
        String cusPath = System.getProperty("user.dir");
        path = path+".png";
        String screenPath = cusPath+"/"+path;
        System.out.println(screenPath);
        //实现截图
        File file = ((TakesScreenshot) webDriver).getScreenshotAs(OutputType.FILE);
        try {
            FileUtils.copyFile(file,new File(screenPath));
        }catch (IOException e){
            e.printStackTrace();
        }
    }

}
