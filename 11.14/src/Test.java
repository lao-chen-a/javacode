class Animal{
    public String name;
    public int age;

    public Animal(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public void print(){
        System.out.print(name+" "+age);

    }
}
class Dog extends Animal{
    public String color;
    public Dog(String name, int age, String color) {
        super(name, age);
        this.color=color;
    }
    public void WangWang(){
        System.out.println("汪汪叫");
    }
}
class Cat extends Animal{
    public Cat(String name, int age) {
        super(name, age);
    }
    public void MiaoMiao(){
        System.out.println("喵喵叫");
    }
}

public class Test {
    public static void main(String[] args) {
        Dog dog=new Dog("旺财",2,"白色");
        dog.WangWang();
        dog.print();
        Cat cat=new Cat("进宝",1);
        cat.print();
        cat.MiaoMiao();
    }
};